var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight){
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    } 
  });
}

function theme(){
	if (document.getElementById("css").href == "{% static 'style.css' %}"){
		document.getElementById("css").href = "{% static 'style2.css' %}";
		}
	else {
		(document.getElementById("css").href = "{% static 'style.css' %}")
}