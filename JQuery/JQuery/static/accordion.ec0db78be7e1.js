 myVar;

function myFunction() {
    myVar = setTimeout(showPage, 3000);
}

function showPage() {
  document.getElementById("loader").style.display = "none";
  document.getElementById("myDiv").style.display = "block";
}

function theme() {
	if (document.getElementById("change_theme").href === "{% static 'style.css' %}"){
		document.getElementById("change_theme").href = "{% static 'stylesheet1.css' %}";
	} else {
		document.getElementById("change_theme").href = "{% static 'style.css' %}";
	}
}

$( function() {
	$("#accordion").accordiion();
});