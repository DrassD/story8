from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest

# Create your tests here.
class StatusTestCase(TestCase):
    def test_url_is_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    def template_is_used(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'accordion.html')